<?php
/** The main template file. 加盟薬局ログインページ */
?>

<div id="contents">
	<?php if (is_user_logged_in()) : // ログイン中 ?>
	
	<div class="bc_F6F8FA ">
		<section class="mem-intro">
			<h2><?php global $current_user; echo $current_user->nickname; // 加盟薬局名 表示 ?></h2>
			<div class="mem-nav">
				<ul>
					<li>
						<a class="bt-box guide" href=""><span class="icon-ic icon-ic-stack"></span><span>ご利用ガイド</span></a>
					</li>
					<li>
						<a class="bt-box guide" href=""><span class="icon-ic icon-ic-file"></span><span>不動品一覧</span></a>
					</li>
					<li>
						<a class="bt-box guide" href=""><span class="icon-ic icon-ic-quest"></span><span>Q & A</span></a>
					</li>
				</ul>
			</div>
		</section>
		
		<section class="mem-topics">
			<h2><span class="icon-ic-hexa"></span><span class="jpn">最新情報</span><span class="eng">INFORMATION</span></h2>
			<div class="u-l"></div>
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				$args = array(
				     'post_type' => 'post', // 投稿タイプを指定
				     'posts_per_page' => 5, // 表示するページ数
				     'paged' => $paged, // 表示するページ数
			); ?>
			<?php $wp_query = new WP_Query( $args ); ?><!-- クエリの指定 -->
				<ul class="topics-list">
				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<li>
						<p class="p-date"><?php the_time( 'Y.m.d （D）' ); ?></p>
						<a class="p-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php wp_reset_postdata(); ?>
		</section>
		<!-- // .mem-topics end -->
		 
	</div>
	<!-- // .bc_F6F8FA end -->

	<div class="bc_E1E5D6 ">
	
		<a id="ask-form" name="ask-form"></a>
		<section class="ask-form">
			<h2>不動在庫移動申込フォーム</h2>
			<form class="apply-form">
				<div class="unit apply-unit">
					
				<dl class="st01">
					<dd class="w-200">
						<input type="text" name="m_name" id="m_name" size="40" value="" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
					</dd>
					<dd class="w-200">
						<input type="text" name="j_code" id="j_code" size="40" value="" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</dd>
				</dl><!— //1段目 END —>
				<dl class="st02">
					<dd class="w-160">
						<input type="text" name="m_company" id="m_company" size="28" placeholder="製造会社" value="" aria-required="true" aria-invalid="false"/>
					</dd>
					<dd class="w-80">
						<select name="slc_dev" class="validate[required]" aria-invalid="false">
							<option value="先発">先発</option>
							<option value="後発">後発</option>
						</select>
					</dd>
					<dd class="w-80">
						<select name="slc_dev" class="validate[required]" aria-invalid="false">
							<option value="内服">内服</option>
							<option value="外用">外用</option>
						</select>
					</dd>
				</dl><!— //2段目 END —>
				<dl class="st03">
					<dd class="w-120">
						<input type="text" name="m_norm" id="m_norm" size="40" placeholder="規格" value="" aria-required="true" aria-invalid="false"/>
						</dd>
					<dd class="w-120">
						<input type="text" name="m_pac" id="m_pac" size="40" placeholder="包装形態" value="" aria-required="true" aria-invalid="false"/>
					</dd>
					<dd class="w-120">
						<input type="text" name="p_quant" id="p_quant" size="40" placeholder="包装数量" value="" aria-required="true" aria-invalid="false"/>
					</dd>
					<dd class="w-136">
						<input type="text" name="b_quant" id="b_quant" size="40" placeholder="売却数量 (総数)" value="" aria-required="true" aria-invalid="false"/>
					</dd>
				</dl><!— //3段目 END —>
				<dl class="st04">
					<dd class="w-200">
						<input type="text" name="m_price" id="m_price" size="40" placeholder="薬価 (小数点２位迄 記入)" value="" aria-required="true" aria-invalid="false"/>
					</dd>
					<dd class="w-160">
						<input type="text" name="experiod" id="experiod" size="40" placeholder="使用期限" value="" aria-required="true" aria-invalid="false"/>
					</dd>
				</dl><!— //4段目 END —>
				<dl class="st05">
					<dd class="">
						<input type="textarea" name="m_note" id="m_note" cols="50" rows="5" placeholder="備考内容" value="" />
					</dd>
				</dl><!— //4段目 END —>
				</div><!— // .unit END —>
				
				<div class="submit-area clearfix">
					<div class="add">
						<input type="button" name="add_to" value="＋薬品を追加" class="bt-submit bt-add" />
					</div>	
					<div class="submit">
						<input id="submit" type="submit" value="確認画面へ進む" class="bt-submit" />
					</div>
				</div><!— // .submit-ara END —>
			</form>

		</section>
	</div>
	<!-- // .bc_E1E5D6 end -->

	<?php else : // ログインしていないとき ?>
	
	<div class="bc_E1E5D6 ">
		
		<section class="mem-login">
			<h2>
				<div class="ic"><span class="icon-ic icon-ic-enter"></span></div>
				<span class="txt">加盟薬局ログイン</span>
			</h2>
			
			<?php // echo do_shortcode('[wp-members page="login"]'); ?>

			<?php echo do_shortcode('[wp-members page="members-area"]'); ?>
			
<script>
jQuery(document).ready(function(){
    jQuery('#log').attr('placeholder', 'ユーザー名');
    jQuery('#user').attr('placeholder', 'ユーザー名');
    jQuery('#pwd').attr('placeholder', 'パスワード(半角英数)');
    jQuery('#email').attr('placeholder', '登録済みのメールアドレス');
});
</script>
			
		</section>
		
	</div>
	<!-- // .bc_E1E5D6 end -->
	
	<?php endif; ?>
</div><!-- // #contents END -->