<?php
/** The main template file. 投稿ページ */
/* Template Name: 投稿ページ */
get_header(); ?>

<div id="contents">

<?php if (is_user_logged_in()) : // ログイン中 ?>
	<div class="bc_F6F8FA ">
		<section class="p-innr loged">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="p-intro">
				<p class="p-date"><?php the_time( 'Y.m.d （D）' ); ?></p>
				<h2 class="p-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></h2>
			</div>
			<div class="p-content"><?php the_content(); ?></div>
		<?php endwhile; endif; ?>
		</section>
		<!-- // .p-innr end -->
		
		<section class="mem-topics">
			<h2><span class="icon-ic-hexa"></span><span class="jpn">最新情報</span><span class="eng">INFORMATION</span></h2>
			<div class="u-l"></div>
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				$args = array(
				     'post_type' => 'post', // 投稿タイプを指定
				     'posts_per_page' => 5, // 表示するページ数
				     'paged' => $paged, // 表示するページ数
			); ?>
			<?php $wp_query = new WP_Query( $args ); ?><!-- クエリの指定 -->
				<ul class="topics-list">
				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<li>
						<p class="p-date"><?php the_time( 'Y.m.d （D）' ); ?></p>
						<a class="p-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php wp_reset_postdata(); ?>
		</section>
		<!-- // .mem-topics end -->
	</div>
	<!-- // .bc_F6F8FA end -->
<?php else : // ログインしていないとき ?>
	<div class="bc_F6F8FA ">
		<section class="p-innr">
			<h2>ログインしてください</h2>
			<div class="b-c">
				このページは、アクセス制限されており、加盟薬局店のみ閲覧可能となります。<br />
				加盟薬局店の方は<a href="<?php echo esc_url( home_url()); ?>/mypage">ログイン</a>をしてください。<br />
				新規に加盟をご希望される方は、<a href="<?php echo esc_url( home_url()); ?>/#c-form">お問合わせフォーム</a>からお問合わせください。
				</div>
		</section>
		<!-- // .p-innr end -->
	</div>
	<!-- // .bc_F6F8FA end -->
<?php endif; ?>

</div>
<!-- // #contents -->

<?php get_footer(); ?>