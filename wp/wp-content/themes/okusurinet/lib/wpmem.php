<?php
	
add_filter( 'wpmem_login_form_rows', 'my_login_form_rows_filter', 10, 2 );
function my_login_form_rows_filter( $rows, $action ) {

    $rows[0][label] = '';
    $rows[1][label] = '';
    // $rows[0][field] = '<input name="log" type="text" id="log" value="" class="username" placeholder="ユーザー名">';
    // $rows[1][field] = '<input name="pwd" type="password" id="pwd" class="password" placeholder="パスワード（半角英数字）">';
     
    return $rows;
}

add_filter( 'wpmem_default_text_strings', 'my_text_strings' );
function my_text_strings( $args )
{
    /*　基本用語の変更　*/
    $args = array(
    	'button_text' => "ログイン",
    );
    return $args;
}

add_filter( 'wpmem_forgot_link_str', 'my_forgot_link_str', 10, 2 );
function my_forgot_link_str( $str, $link ) {
	return "パスワードを忘れた方は<a href=\"$link\">こちら</a>";
}

add_filter( 'wpmem_reg_link_str', 'my_reg_link_str', 10, 2 );
function my_reg_link_str( $str, $link ) {
	return "";
}

add_filter( 'wpmem_inc_resetpassword_args', 'my_resetpassword_args' );
function my_resetpassword_args( $args )
{
    /* パスワードを再設定 */
    $args = array(
        'heading'      => "パスワード再設定",
        'button_text'  => "パスワード変更"
    );
    return $args;
}


?>