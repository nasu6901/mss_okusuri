<?php
/** The main template file. HOMEページ */
/* Template Name: HOMEページ */
get_header(); ?>


<div class="kv">
	
	<?php include(TEMPLATEPATH . '/temp/temp_g_nav.php'); ?>
	
	<div id="Glide" class="glide">
		<div class="glide__wrapper">
			<ul class="glide__track">
				<section>
				<li class="glide__slide">
					<div class="slide slide-01">
						<img class="w-max" src="<?php echo get_template_directory_uri(); ?>/images/slider/img_kv-slider_01.png" srcset="<?php echo get_template_directory_uri(); ?>/images/slider/img_kv-slider_01.png 1x,<?php echo get_template_directory_uri(); ?>/images/slider/img_kv-slider_01@2x.png 2x" alt="" />
					</div>
				</li>
				<li class="glide__slide">
					<div class="slide slide-02">
						<img class="w-max" src="<?php echo get_template_directory_uri(); ?>/images/slider/img_kv-slider_02.png" srcset="<?php echo get_template_directory_uri(); ?>/images/slider/img_kv-slider_02.png 1x,<?php echo get_template_directory_uri(); ?>/images/slider/img_kv-slider_02@2x.png 2x" alt="" />
<!--
						<div class="intro">
							<h2>只今関東を中心に加盟店増加中</h2>
							<p class="b-c">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
						</div>
						<img class="img-02" src="<?php echo get_template_directory_uri(); ?>/images/slider/img_kv-0yen.png" alt="" />
-->
					</div>
				</li>
<!--
				<li class="glide__slide">何ぬねの</li>
-->
				</section>
			</ul>
		</div>
		<div class="glide__bullets"></div>
	</div>
</div>
<!-- // .kv END-->

<script>
    $("#Glide").glide({
        type: "slideshow"
    });
</script>

<div id="contents">
	<section class="service">
		<img class="img-01" src="<?php echo get_template_directory_uri(); ?>/images/page/img_service.png" srcset="<?php echo get_template_directory_uri(); ?>/images/page/img_service.png 1x,<?php echo get_template_directory_uri(); ?>/images/page/img_service@2x.png 2x" alt="" />
		<h2>お薬NETで、経営コストを削減しませんか？</h2>
		<p class="b-c">調剤薬局の経営において、もともと価格交渉や在庫管理、支払いといった間接コストには問題を感じにくいかもしれません。お薬NETでは、その見えにくい人件費や販管費に対して、経営コストの削減をご提案いたします。</p>
	</section>
	
	<div class="bc_4DBA95">
		<section class="benefit">
			<h2>お薬NET<strong>4</strong>つのベネフィット</h2>
			<ul>
				<li>
					<section>
						<h3>① 仕入れの価格交渉が不要</h3>
						<p class="c-c">交渉をお薬NETで代行し、独自の価格設定で仕入れコスト削減</p>
						<div class="u-l"></div>
						<p class="note">卸会社との駆け引きや価格を交渉する必要がなくなります。お薬NET独自の価格設定により、仕入れを優遇します。</p>
					</section>
				</li>
				<li>
					<section>
						<h3>② 支払いの一本化</h3>
						<p class="c-c">当社への支払い一本化で効率化。納価チェックサービスも実施</p>
						<div class="u-l"></div>
						<p class="note">各問屋への支払いは当社から行いますので、卸会社ごとに振込む手間がなくなり、当社への一括支払いのみとなります。</p>
					</section>
				</li>
				<li>
					<section>
						<h3>③ 不動在庫の移動</h3>
						<p class="c-c">加盟グループ店舗間での不動在庫を仲介</p>
						<div class="u-l"></div>
						<p class="note">当社ネットワークを活用し、有効期限が近づいたお薬や不動となった在庫をお薬NET内の薬局間で移動ができるようになります。</p>
					</section>
				</li>
				<li>
					<section>
						<h3>④ 発注作業の効率化</h3>
						<p class="c-c">インターネットによる電子送信で効率化、不動在庫・期限切れなどを日常的に管理</p>
						<div class="u-l"></div>
						<p class="note">たとえば、開封後の空箱のバーコードをスキャンしてインターネットで送信するだけの作業になり、管理業務が大幅に効率化します。急を要する場合の電話やFAXによる発注にも対応しております。</p>
					</section>
				</li>
			</ul>
		</section>
	</div><!--  // .bc_4DBA95  -->
	
	<div class="bc_F6F8FA">
		<section class="flow">
			<h2>加盟までの流れ</h2>
			<ul>
				<li>
					<section>
						<div class="ic-flow">
							<img src="<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-01.png" srcset="<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-01.png 1x,<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-01@2x.png 2x" alt="" />
						</div>
						<h3>お問い合わせ</h3>
						<p class="note">問い合わせフォームまたは、メール・電話などで、弊社にご連絡</p>
					</section>
				</li>
				<li>
					<section>
						<div class="ic-flow">
							<img src="<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-02.png" srcset="<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-02.png 1x,<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-02@2x.png 2x" alt="" />
						</div>
						<h3>内容説明</h3>
						<p class="note">詳細な内容説明、加盟後の差益シミュレーション等のご説明</p>
					</section>
				</li>
				<li>
					<section>
						<div class="ic-flow">
							<img src="<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-03.png" srcset="<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-03.png 1x,<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-03@2x.png 2x" alt="" />
						</div>
						<h3>各種調整</h3>
						<p class="note">加盟の決定後、取引卸との各種調整、システム整備</p>
					</section>
				</li>
				<li>
					<section>
						<div class="ic-flow">
							<img src="<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-04.png" srcset="<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-04.png 1x,<?php echo get_template_directory_uri(); ?>/images/page/ic_flow-04@2x.png 2x" alt="" />
						</div>
						<h3>ご契約</h3>
						<p class="note">契約書締結後に、お取引開始</p>
					</section>
				</li>
			</ul>
		</section>
	</div><!--  // .bc_F6F8FA  -->
	
	<a id="c-form" name="c-form"></a>
	<div class="bc_E1E5D6 ">
		<section class="contact">
			<h2>お問合わせ</h2>
			<div class="ic"><span class="icon-ic icon-ic-mail"></span></div>
			<h3>メールでのお問い合わせ</h2>
			<p class="note">お問い合わせフォームをご利用ください。弊社の担当者が、後ほど連絡いたします。<br />なお、お問い合わせの前に「プライバシーポリシー」をご覧ください。<br />※ご入力いただく個人情報の送信については、SSLによる暗号化でセキュリティを保たれています。</p>

			<script>
				jQuery(document).ready(function(){
					jQuery("form").validationEngine();
					jQuery("#your-cmp").addClass("validate[required]");
					jQuery("#your-name").addClass("validate[required]");
					jQuery("#your-txt").addClass("validate[required]");
					jQuery("#your-mail").addClass("validate[required,custom[email]]");
					// jQuery("#your-tel").addClass("validate[required,custom[phone]]");							
				});
			</script>
			
			<?php echo do_shortcode( '[mwform_formkey key="4"]' ); ?>
			
		</section>
	</div><!--  // .bc_E1E5D6   -->
	
</div>
<!-- // #contents -->

<?php get_footer(); ?>