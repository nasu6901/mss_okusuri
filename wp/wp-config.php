<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'msnw_okusurinet');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'msnw_webmaster');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'ZNYZ6SUx');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql2303.xserver.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6,Rw;sm1E.yE_%y,72QKYqW%561_cl?3p#K@?Zh<28aJSm1mpQU+9`N3$Dd]%#3r');
define('SECURE_AUTH_KEY',  'rC?9UCUlll+MC[HGjSeYb|b&h l?A#?,+A-~Xja1qM@~Z89n`H3HMG%G~~*va$( ');
define('LOGGED_IN_KEY',    'R.wh{SV$wiw-e,Va,K`vJ$VL#!1a*i{LU`AqM(_7#%l]^Ke-q5n]3G/_;JeYf&mt');
define('NONCE_KEY',        'FY)Q @:0>U=(9FDWafhLI.YtK:>RcQu4`Fidf A)oC=w@$-fDuBW7$ 8=5!.[SLm');
define('AUTH_SALT',        'LDN<V$p(NY.~!Dwm&jFC!8uMgx-[TmR5Anz:xHLfC+Kq6N0#+.:*wI?KnU=g0yWB');
define('SECURE_AUTH_SALT', 'ScN5z/@ZEfGnzYniu*O[3%A0GHL8UBI%k8iCtaiM/Xh70vJ2]0wgmF<89]HGZt*.');
define('LOGGED_IN_SALT',   '8~no+jY@NW[dgH#k<nr-:YOb?zeFlRClfr+F]}n%H<Ft[L@v#>RG;UyswPt_AdX`');
define('NONCE_SALT',       'XRz^Tsx3cZ_%BkVi]k2d5/4:4QK#G~.SS7rjg.Bv9(HV6[bz)@ :6TdD+oQ>>1]}');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
